@extends('layout.default')

@section('page-title')
{{ $user->name }}
@stop

@section('scripts')
@stop

@section('content')
<div class="features_items">
    <!--features_items-->
    <h2 class="title text-center">Filmes</h2>

    @foreach($movies as $k => $movie)
        <div class="col-sm-3 col-md-3 col-xs-6">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img src="https://graph.facebook.com/{{ $movie->facebook_id }}/picture?width=200&height=299" alt="" />
                        <p>{{ $movie->title }}</p>
                        @for($i = 0; $i < $movie->rating; $i++)
                        <i class="fa fa-star" style="color: #FFD700"></i>
                        @endfor
                        @for(; $i < 5; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                    </div>
                    <a href="/movies/{{ $movie->id }}">
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <p>{{ $movie->abstract }}</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        @if(($k+1) % 4 == 0)
            <div class="clearfix visible-md-block visible-lg-block"></div>
        @elseif(($k+1) % 3 == 0)
            <div class="clearfix visible-sm-block"></div>
        @elseif(($k+1) % 2 == 0)
            <div class="clearfix visible-xs-block"></div>
        @endif
    @endforeach

    <div class="row">
        <div class="col-sm-12">
            <ul class="pagination">
                {{ $movies->links() }}
            </ul>
        </div>
    </div>
</div>
@stop