@extends('layout.default')

@section('page-title')
{{ $user->name }}
@stop

@section('scripts')
@stop

@section('content')
<div class="features_items">
    <!--features_items-->
    <h2 class="title text-center">Perfil de {{ $user->name }}</h2>

    @if(count($movies_genres) <= 0 && count($books_genres) <= 0)
        <div class="row">

            <div class="col-sm-12">
                <p>Suas informações não foram coletadas ainda.</p>
                <p>Clique no botão "Coletar Meus Filmes e Livros" ao lado.</p>
            </div>
        </div>
    @endif

    @if(count($movies_genres) > 0)
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <tr>
                    <th>Filme</td>
                    <th>Percentual</td>
                </tr>
                @for($k = 0; $k < 5 ; $k++)
                    <tr>
                        <td class="col-md-8">{{ $movies_genres[$k]->name }}</td>
                        <td class="col-md-4">{{ $movies_genres[$k]->percent }}</td>
                    </tr>
                @endfor
            </table>
        </div>
    </div>
    @endif

    <hr>

    @if(count($books_genres) > 0)
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped">
                <tr>
                    <th>Livro</td>
                    <th>Percentual</td>
                </tr>
                @for($k = 0; $k < 5 ; $k++)
                    <tr>
                        <td class="col-md-8">{{ $books_genres[$k]->name }}</td>
                        <td  >{{ $books_genres[$k]->percent }}</td>
                    </tr>
                @endfor
            </table>
        </div>
    </div>
    @endif

</div>
@stop