@extends('layout.default')

@section('page-title')
{{ $user->name }}
@stop

@section('scripts')
@stop

@section('content')
<div class="features_items">
    <!--features_items-->
    <h2 class="title text-center">Livros</h2>

    @foreach($books as $k => $book)
        <div class="col-sm-4 col-md-3 col-xs-6">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img src="https://graph.facebook.com/{{ $book->facebook_id }}/picture?width=200&height=299" alt="" />
                        <p>{{ $book->title }}</p>
                        @for($i = 0; $i < $book->rating; $i++)
                        <i class="fa fa-star" style="color: #FFD700"></i>
                        @endfor
                        @for(; $i < 5; $i++)
                        <i class="fa fa-star"></i>
                        @endfor
                    </div>
                    <a href="/books/{{ $book->id }}">
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <p>{{ $book->abstract }}</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        @if(($k+1) % 4 == 0)
            <div class="clearfix visible-md-block visible-lg-block"></div>
        @elseif(($k+1) % 3 == 0)
            <div class="clearfix visible-sm-block"></div>
        @elseif(($k+1) % 2 == 0)
            <div class="clearfix visible-xs-block"></div>
        @endif
    @endforeach

    <div class="row">
        <div class="col-sm-12">
            <ul class="pagination">
                {{ $books->links() }}
            </ul>
        </div>
    </div>
</div>
@stop