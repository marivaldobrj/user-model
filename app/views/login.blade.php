<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>User Model</title>
    {{ HTML::style('assets/css/bootstrap.min.css') }}
    {{ HTML::style('assets/css/font-awesome.min.css') }}
    {{ HTML::style('assets/css/bigvideo.css') }}
    {{ HTML::style('assets/usermodel/css/login.css') }}
</head>
<body style="background-image: url( {{ URL::to('assets/images/bookcase.jpg') }}); background-repeat: repeat-x; background-position: center top;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="page-section">
                    <h2>
                        Modelo de Usuário Automatizado
                    </h2>

                    <a href="{{ URL::to('/user/facebook') }}" class="btn btn-block btn-social btn-facebook" style="text-align: center; font-weight: bold;">
                        <i class="fa fa-facebook"></i> Login Facebook
                    </a>
                </div>

            </div>
        </div>
    </div>
</body>

    {{ HTML::script('assets/js/jquery.min.js') }}
    {{ HTML::script('assets/js/jquery-ui.min.js') }}
</html>
