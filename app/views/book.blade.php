@extends('layout.default')

@section('page-title')
{{ $book->title }}
@stop

@section('scripts')
@stop

@section('content')
<div class="features_items">
    <!--features_items-->
    <h2 class="title text-center">Livro</h2>

    <div class="col-sm-3 col-md-3 col-xs-6" style="width: 100%">
        <div class="pull-left">
            <img src="https://graph.facebook.com/{{ $book->facebook_id }}/picture?width=200&height=312" class="img-thumbnail" />
        </div>
        <div style="padding: 0 10px; overflow: hidden">
            <div class="pull-right" style="margin-top: 15px">
                @for($i = 0; $i < $book->rating; $i++)
                <i class="fa fa-star" style="color: #FFD700"></i>
                @endfor
                @for(; $i < 5; $i++)
                <i class="fa fa-star"></i>
                @endfor
            </div>
            <h1 style="margin: 0">{{ $book->title }} <small>{{ $book->year }}</small></h1>

            <p class="lead">
                Autor: {{ $book->author }}
            </p>

            <p>{{ $book->abstract }}</p>

            <p><strong>País:</strong> {{ $book->country->name }}</p>

            <p><strong>Gêneros:</strong> {{ $book->genresToString() }}</p>

            <p><strong>Data de Lançamento:</strong> {{ date('d/m/Y', strtotime( $book->release_date )) }}</p>
        </div>

    </div>
</div>
@stop