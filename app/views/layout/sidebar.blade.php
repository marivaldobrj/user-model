<div class="left-sidebar">

    <h2>{{ Auth::user()->first_name }}</h2>
    <div class="panel-group category-products" style="text-align: center">
        <img src="https://graph.facebook.com/{{ Auth::user()->facebook_id }}/picture?width=150&height=150" alt="User picture" title="{{ Auth::user()->name }}" />
    </div>

    <div class="brands_products">
        <div class="brands-name">
            <ul class="nav nav-pills nav-stacked">
                @if(Auth::user()->facebook_data_status == 'synced')
                <li {{ (Request::is('users/*/movies') ? ' class="active"' : '') }}><a href="{{ URL::to('users/'.Auth::user()->id.'/movies') }}"> <span class="pull-right">({{ Auth::user()->movies->count() }})</span>Filmes</a></li>
                <li {{ (Request::is('users/*/books') ? ' class="active"' : '') }}><a href="{{ URL::to('users/'.Auth::user()->id.'/books') }}"> <span class="pull-right">({{ Auth::user()->books->count() }})</span>Livros</a></li>
                @else
                <li id="get-facebook-data"><a class="btn btn-default" href="{{ URL::to('user/facebook-data') }}">Coletar Meus Filmes e Livros</a></li>
                @endif
            </ul>
        </div>
    </div>

</div>