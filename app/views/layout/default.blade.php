<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('meta-description')">

    @yield('meta')

    <link rel="icon" href="{{ asset('assets/images/favicon.png') }}" />

    <title>@yield('page-title')</title>

    {{ HTML::style('assets/css/bootstrap.min.css') }}
    {{ HTML::style('assets/css/font-awesome.min.css') }}
    {{ HTML::style('assets/css/prettyPhoto.css') }}
    {{ HTML::style('assets/css/price-range.css') }}
    {{ HTML::style('assets/css/animate.css') }}
    {{ HTML::style('assets/css/main.css') }}
    

    {{HTML::style('assets/usermodel/css/default.css')}}

    @yield('css')
    <!--[if lt IE 9]>
    {{ HTML::script('assets/js/jquery-ui.min.js') }}
    {{ HTML::script('assets/js/respond.min.js') }}
    <![endif]-->
</head><!--/head-->

<body>
    @if(Auth::check())
        @include('layout.navbar')
    @endif
<section>
    <div class="container">
        <div class="row">
            @if(Request::is('/'))

                <div class="col-sm-12 padding-right">
                    @yield('content')
                </div>
            @else

                @if(Auth::check())
                <div class="col-sm-3">
                    @include('layout.sidebar')
                </div>
                @endif

                <div class="col-sm-9 padding-right">
                    @yield('content')
                </div>

            @endif
        </div>
    </div>
</section>

{{-- @include('layout.footer') --}}


{{ HTML::script('assets/js/jquery.min.js') }}
{{ HTML::script('assets/js/price-range.js') }}
{{ HTML::script('assets/js/jquery.scrollUp.min.js') }}
{{ HTML::script('assets/js/bootstrap.min.js') }}
{{ HTML::script('assets/js/jquery.prettyPhoto.js') }}
{{ HTML::script('assets/js/main.js') }}
{{ HTML::script('assets/usermodel/js/default.js') }}
@yield('scripts')
</body>
</html>