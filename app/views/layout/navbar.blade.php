
<header id="header"><!--header-->
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{ URL::to('/') }}">Modelo de Usuário Automatizado</a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ URL::to('users/'.Auth::user()->id) }}"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a></li>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-cog"></i> Configurações</a></li>
                                    <li><a href="{{ URL::to('user/logout') }}">Sair</a></li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a {{ (Request::is('/') ? ' class="active"' : '') }} href="{{ URL::to('/') }}">Home</a></li>
                            <li><a {{ (Request::is('users/*/movies') ? ' class="active"' : '') }} href="{{ URL::to('users/'.Auth::user()->id.'/movies') }}">Filmes</a></li>
                            <li><a {{ (Request::is('users/*/books') ? ' class="active"' : '') }} href="{{ URL::to('users/'.Auth::user()->id.'/books') }}">Livros</a></li>
                            <li><a {{ (Request::is('users/*/profile') ? ' class="active"' : '') }} href="{{ URL::to('users/'.Auth::user()->id.'/profile') }}">Perfil</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>