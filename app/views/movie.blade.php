@extends('layout.default')

@section('page-title')
{{ $movie->title }}
@stop

@section('scripts')
@stop

@section('content')
<div class="features_items">
    <!--features_items-->
    <h2 class="title text-center">Filme</h2>

    <div class="col-sm-3 col-md-3 col-xs-6" style="width: 100%">
        <div class="pull-left">
            <img src="https://graph.facebook.com/{{ $movie->facebook_id }}/picture?width=200&height=312" class="img-thumbnail" />
        </div>
        <div style="padding: 0 10px; overflow: hidden">
            <div class="pull-right" style="margin-top: 15px">
                @for($i = 0; $i < $movie->rating; $i++)
                <i class="fa fa-star" style="color: #FFD700"></i>
                @endfor
                @for(; $i < 5; $i++)
                <i class="fa fa-star"></i>
                @endfor
            </div>
            <h1 style="margin: 0">{{ $movie->title }} <small>{{ $movie->year }}</small></h1>

            <p class="lead">
                Dirigido por: {{ $movie->director }}
            </p>

            <p>{{ $movie->abstract }}</p>

            <p><strong>País:</strong> {{ $movie->country->name }}</p>

            <p><strong>Gêneros:</strong> {{ $movie->genresToString() }}</p>

            <p><strong>Data de Lançamento:</strong> {{ date('d/m/Y', strtotime( $movie->release_date )) }}</p>
        </div>

    </div>
</div>
@stop