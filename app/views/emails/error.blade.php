<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css">
             /* Client-specific Styles */
             #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
             body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
             /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
             .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
             .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  */
             #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
             img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
             a img {border:none;}
             .image_fix {display:block;}
             p {margin: 0px 0px !important;}
             table td {border-collapse: collapse;}
             table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
             a {color: #0068cf;text-decoration: none;text-decoration:none!important;}
             /*STYLES*/
             table[class=full] { width: 100%; clear: both; }
             /*IPAD STYLES*/
             @media only screen and (max-width: 640px) {
             a[href^="tel"], a[href^="sms"] {
             text-decoration: none;
             color: #0068cf; /* or whatever your want */
             pointer-events: none;
             cursor: default;
             }
             .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
             text-decoration: default;
             color: #0068cf !important;
             pointer-events: auto;
             cursor: default;
             }
             table[class=devicewidth] {width: 440px!important;text-align:center!important;}
             td[class=devicewidth] {width: 440px!important;text-align:center!important;}
             img[class=devicewidth] {width: 440px!important;text-align:center!important;}
             img[class=banner] {width: 440px!important;height:147px!important;}
             table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
             table[class=icontext] {width: 345px!important;text-align:center!important;}
             img[class="colimg2"] {width:420px!important;height:243px!important;}
             table[class="emhide"]{display: none!important;}
             img[class="logo"]{width:440px!important;height:110px!important;}

             }
             /*IPHONE STYLES*/
             @media only screen and (max-width: 480px) {
             a[href^="tel"], a[href^="sms"] {
             text-decoration: none;
             color: #0068cf; /* or whatever your want */
             pointer-events: none;
             cursor: default;
             }
             .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
             text-decoration: default;
             color: #0068cf !important;
             pointer-events: auto;
             cursor: default;
             }
             table[class=devicewidth] {width: 280px!important;text-align:center!important;}
             td[class=devicewidth] {width: 280px!important;text-align:center!important;}
             img[class=devicewidth] {width: 280px!important;text-align:center!important;}
             img[class=banner] {width: 280px!important;height:93px!important;}
             table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
             table[class=icontext] {width: 186px!important;text-align:center!important;}
             img[class="colimg2"] {width:260px!important;height:150px!important;}
             table[class="emhide"]{display: none!important;}
             img[class="logo"]{width:280px!important;height:70px!important;}

             }
        </style>
    </head>
    <body>
        <!-- Start of LOGO -->
        <table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
           <tbody>
              <tr>
                 <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                       <tbody>
                          <tr>
                             <td width="100%">
                                <table bgcolor="#e8eaed" width="100%" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                   <tbody>
                                      <tr>
                                         <!-- start of image -->
                                         <td align="center" height="150" bgcolor="#CA5F58">
                                            <h1 style="font-family: Helvetica, arial, sans-serif; color: #333333; font-size: large; font-weight: bold;">{{ $exception_name }}</h1>
                                            <h2 style="font-family: Helvetica, arial, sans-serif; color: #333333; font-size: large; font-weight: bold;">{{ $error_message }}</h2>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                                <!-- end of image -->
                             </td>
                          </tr>
                       </tbody>
                    </table>
                 </td>
              </tr>
           </tbody>
        </table>
        <!-- End of LOGO -->
        <!-- start textbox-with-title -->
        <table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
           <tbody>
              <tr>
                 <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                       <tbody>
                          <tr>
                             <td width="100%">
                                <table bgcolor="#ccc" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                   <tbody>
                                      <!-- Spacing -->
                                      <tr>
                                         <td width="100%" height="20"></td>
                                      </tr>
                                      <!-- Spacing -->
                                      <tr>
                                         <td>
                                            <table width="95%" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                               <tbody>
                                                  <table bgcolor="#A6A6A6" width="100%" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                                      <!-- Title -->
                                                      <tr>
                                                         <td>
                                                            <p>
                                                                <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Hora:</span>
                                                                <span>{{ Carbon\Carbon::now()->toTimeString() }}</span>
                                                            </p>
                                                            <p>
                                                                <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Código:</span>
                                                                <span>{{ $code }}</span>
                                                            </p>
                                                            <p>
                                                                <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Origem:</span>
                                                                <span>{{ $ip }} ({{ $host }})</span>
                                                            </p>
                                                            <p>
                                                                <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">URL:</span>
                                                                <span>{{ $url }}</span>
                                                            </p>
                                                            @if(Auth::check())
                                                            <p>
                                                                <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Usuário:</span>
                                                                <br />
                                                                <span style="padding-left: 2em; font-weight: bold; color: #353535;">id:</span><span> {{ Auth::user()->id }}</span>
                                                                <br />
                                                                <span style="padding-left: 2em; font-weight: bold; color: #353535;">username:</span><span> {{ Auth::user()->nome_usuario }}</span>
                                                            </p>
                                                            @endif
                                                         </td>
                                                      </tr>
                                                      <!-- End of Title -->
                                                  </table>
                                                  <!-- spacing -->
                                                  <tr>
                                                     <td height="15"></td>
                                                  </tr>
                                                  <!-- End of spacing -->
                                                  <tr>
                                                     <td>
                                                        <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Headers:</span>
                                                        <pre style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; line-height: 24px;">{{ print_r($headers) }}</pre>
                                                     </td>
                                                  </tr>
                                                  <!-- spacing -->
                                                  <tr>
                                                     <td height="15"></td>
                                                  </tr>
                                                  <!-- End of spacing -->
                                                    <tr>
                                                       <td>
                                                          <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Request:</span>
                                                          <pre style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; line-height: 24px;">{{ print_r($request) }}</pre>
                                                       </td>
                                                    </tr>
                                                  <!-- spacing -->
                                                  <tr>
                                                     <td height="15"></td>
                                                  </tr>
                                                  <!-- End of spacing -->
                                                  <!-- content -->
                                                  <tr>
                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #333333; text-align:left;line-height: 24px;">
                                                        <span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: bold; color: #333333; text-align:left;line-height: 24px;">Exception:</span>
                                                        {{ nl2br($exception) }}
                                                     </td>
                                                  </tr>
                                                  <!-- End of content -->
                                                  <!-- Spacing -->
                                                  <tr>
                                                     <td width="100%" height="5"></td>
                                                  </tr>
                                                  <!-- Spacing -->
                                               </tbody>
                                            </table>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                             </td>
                          </tr>
                       </tbody>
                    </table>
                 </td>
              </tr>
           </tbody>
        </table>
        <!-- end of textbox-with-title -->
    </body>
</html>