<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GetMoviesInfoFromDBPedia extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dbpedia:update-movies';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get movies details from DBpedia.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->getMoviesInfo();
	}

	protected function getMoviesInfo()
	{
		$start_total = microtime(true);

		set_time_limit(0);

		include(app_path().'/includes/sparqllib.php');

		$db = sparql_connect( "http://dbpedia.org/sparql" );
		if( !$db ) {
			$this->error(sparql_errno() . ": " . sparql_error());
			exit;
		}

		sparql_ns( "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
		sparql_ns( "rdfs","http://www.w3.org/2000/01/rdf-schema#" );
		sparql_ns( "dbpedia","http://dbpedia.org/resource/" );
		sparql_ns( "dbpedia-owl","http://dbpedia.org/ontology/" );
		sparql_ns( "dbpprop","http://dbpedia.org/property/" );

		// pega todos os generos registrados
		$allGenres = Genre::lists('name', 'id');

		//$movie_list = '';
		$movies = Movie::where('info_status', '=', 'outdated')->orderBy('id')->get();
		$count = 1;
		$total_movies = $movies->count();
		foreach($movies as $movie) {

			$patterns = array(
				'/ \(film series\)/',
				'/\(film series\)/',
				'/ \(film\)/',
				'/\(film\)/',
				'/ \(TV special\)/',
				'/\(TV special\)/',
				'/ \(\d+ film\)/',
				'/\(\d+ film\)/',
				'/ \(\d+\)/'
			);
			$title = preg_replace($patterns, '', $movie->title);
			$title_search = $output = rtrim($title, '.');

			$this->info($count++ . '/' .$total_movies . ' ## ' . $title);
			$this->info('Getting details from DBpedia.');

			$sparql = '
                SELECT DISTINCT
                    str(?name) as ?film_title
                    str(?film_director) as ?film_director
                    str(?film_country) as ?film_country
                    str(?film_released) as ?film_released
                    str(?film_abstract) as ?film_abstract
                    ?film_runtime
                    (group_concat(distinct ?film_genre;separator=",") as ?film_genre)

                    WHERE {
                        { ?film_title rdf:type <http://dbpedia.org/ontology/Film> }
                        UNION
                        { ?film_title rdf:type <http://dbpedia.org/class/yago/AmericanFilms> }
                        ?film_title rdfs:comment ?film_abstract .
                        ?film_title dct:subject/rdfs:label ?film_genre .
                        ?film_title rdfs:label ?name
                        OPTIONAL {
                             { ?film_title dbpedia-owl:director/rdfs:label ?film_director }
                             UNION
                             { ?film_title dbp:director/rdfs:label ?film_director }
                        }
                        OPTIONAL {
                        	?film_title dbpprop:country ?film_country .
                        }
                        OPTIONAL {
                        	?film_title dbpprop:released ?film_released .
                        }
                        OPTIONAL {
                        	?film_title dbpedia-owl:runtime ?film_runtime .
                        }
                        FILTER(LANGMATCHES(LANG(?name), "en")) .
                        FILTER(LANGMATCHES(LANG(?film_director), "en")) .
                        FILTER(LANGMATCHES(LANG(?film_abstract), "en")) .
                        FILTER regex(str(?name), "\\\b'.$title_search.'\\\b", "i")
                    } LIMIT 10';
			//return var_dump($sparql);
			//FILTER contains(str(?name), "'.$title.'")
			//FILTER regex(str(?name), "\\\b'.$title.'\\\b", "i")

			$result = sparql_query( $sparql );
			if( !$result ) {
				$this->error(sparql_errno() . ": " . sparql_error());
				exit;
			}

			// não achou nenhum filme
			if(count($result->rows) <= 0) {
				$this->info('Movie NOT FOUND.' . PHP_EOL);
				continue;
			}

			// verifica cada resultado retornado pelo sparql
			while( $row = sparql_fetch_array( $result ) )
			{
				$db_title = preg_replace($patterns, '', $row['film_title']);
				if(mb_strtolower($db_title) == mb_strtolower($title)) {
					break;
				}
			}

			$genres = explode(',', $row['film_genre']);

			$ids = array();
			foreach($allGenres as $id => $genre){
				$matches = array_filter($genres, function($var) use ($genre) { return preg_match("/\b$genre\b/i", $var); });
				if(!empty($matches)){
					$ids[] = $id;
				}
			}

			if(isset($row['film_country'])){
				$patterns = array(
					'/.*\//',
				);
				$country_name = preg_replace($patterns, '', $row['film_country']);
				$country_name = str_replace('_', ' ', $country_name);
				$this->info($country_name);
				try{
					$country = Country::where('name', $country_name)->firstOrFail();
				}catch (ModelNotFoundException $e){
					$country = new Country();
					$country->name = $country_name;
					$country->save();
				}
				$movie->countries_id = $country->id;
			}else{
				$movie->countries_id = null;
			}

			// ano de quatro digitos
			if( isset($row['film_released']) && preg_match('/^[1-9]\d{3}$/', (int)$row['film_released']) ){
				$row['film_released'] = $row['film_released'] . '-01-01';
			}

			$movie->director = isset($row['film_director']) ? $row['film_director'] : null;
			$movie->release_date = isset($row['film_released']) ? $row['film_released'] : null;
			$movie->abstract = $row['film_abstract'];
			$movie->info_status = 'updated';
			$movie->save();
			$movie->genres()->sync($ids);

			$this->info('Movie updated.' . PHP_EOL);

			//$movie_list .= $title . '<br>';
		}

		$time_elapsed_us = number_format((microtime(true) - $start_total),2,',','.');
		$this->info('Total Elapsed Time: ' . $time_elapsed_us . "s" . PHP_EOL);

		//$this->comunidades()->attach($comunidade->id, array('tipo_usuario_id' => $tipo));

		//return 'movies info updated.';
		//return $movie_list;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
