<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReadDataFile extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'facebook:read-file';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$path = storage_path() . '\\app\\facebook';

		if ($handle = opendir($path)) {

			// le cada arquivo no diretorio
			while (false !== ($entry = readdir($handle))) {
				$file_path = $path.'\\'. $entry;
				// current and parent director e others directories
				if ($entry != "." && $entry != ".." && !is_dir($file_path))
				{
					$this->process_file($entry);
					$this->info(PHP_EOL);
				}
			}

			closedir($handle);
		}

	}

	protected function process_file($entry)
	{
		$path = storage_path() . '\\app\\facebook';
		$file_path = $path.'\\'. $entry;

		$handle = fopen($file_path, "r");
		$line_number = 0;
		$tipo_dado = '';
		$movies = array();
		$books = array();
		if ($handle) {

			$data_facebook = array();
			$lendo_url = true;
			$indice = 0;

			// le cada linha do arquivo
			while (($line = fgets($handle)) !== false) {
				$line_number++;

				#ler informacoes do usuario
				if($line_number == 1){
					$dados = str_replace(PHP_EOL, '', $line);
					$dados = explode('/', $dados);

					$nome = $dados[0];
					$facebook_id = $dados[1];
					$tipo_dado = $dados[2];

					try{
						$user = User::where('name', $nome)->firstOrFail();

						if ($user && $tipo_dado == 'books'){
							if ($user->books->count() > 0){
								return;
							}
						}else if($user && $tipo_dado == 'movies' ) {
							if ($user->movies->count() > 0){
								return;
							}
						}
						$this->info('PROCESSANDO: ' . $entry);
					}catch (ModelNotFoundException $e){
						$this->info('PROCESSANDO: ' . $entry);
						$user = new User();
						$user->name = $nome;
						$user->facebook_id = $facebook_id;
						$user->save();
					}

					continue;
				}

				// verifica se já leu a ultima url
				if (trim($line) == '##'){
					$lendo_url = false;
					$indice = 0;
					continue;
				}

				if ($lendo_url) {
					preg_match_all('/(.*)\?/', $line, $matches, PREG_SET_ORDER);
					/*$title = explode('/', $matches[0][1]);
					try{
						$title = $title[4];
					}catch (Exception $e){
						$title = $title[3];
					}
					*/
					$filme = new stdClass();
					$filme->url = $matches[0][1];

					$data_facebook[$indice++] = $filme;
				}else{
					$patterns = array(
						'/ \(short story collection\)/',
						'/\(short story collection\)/',
						'/ \(series\)/',
						'/\(series\)/',
						'/ \(book\)/',
						'/\(book\)/',
						'/ \(novel\)/',
						'/ \([A-Za-z]* novel\)/',
						'/\(novel\)/',
						'/ \(\d+ book\)/',
						'/\(\d+ book\)/',

						'/ \(film series\)/',
						'/\(film series\)/',
						'/ \(film\)/',
						'/\(film\)/',
						'/ \(TV special\)/',
						'/\(TV special\)/',
						'/ \(\d+ film\)/',
						'/\(\d+ film\)/',
						'/ \(\d+\)/'
					);
					$title = preg_replace($patterns, '', $line);
					$data_facebook[$indice++]->title = $title;
				}
			}
			//file_put_contents(storage_path('test.txt'), print_r($data_facebook, true));

			foreach($data_facebook as $data){
				$this->info(trim($data->title));

				if($tipo_dado == 'books') {
					try{
						$book = Book::where('title', trim($data->title))->firstOrFail();
					}catch (ModelNotFoundException $e){
						$book = new Book();
						$book->title = trim($data->title);
						$book->facebook_url = trim($data->url);
						$book->info_status = 'outdated';
						$book->save();
					}

					$books[] = $book;
				}else {
					try{
						$movie = Movie::where('title', trim($data->title))->firstOrFail();
					}catch (ModelNotFoundException $e){
						$movie = new Movie();
						$movie->title = trim($data->title);
						$movie->facebook_url = trim($data->url);
						$movie->info_status = 'outdated';
						$movie->save();
					}

					$movies[] = $movie;
				}
			}

			if(isset($user)){
				if($tipo_dado == 'books') {
					$user->books()->saveMany($books);
				}else{
					$user->movies()->saveMany($movies);
				}
			}else{
				$this->error('User not setted.');
				exit;
			}

			fclose($handle);
			rename($file_path, storage_path() . '\\app\\facebook\\processed\\'. $entry);
		} else {
			// error opening the file.
		}
	}



	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
