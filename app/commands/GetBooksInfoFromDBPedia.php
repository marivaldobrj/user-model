<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GetBooksInfoFromDBPedia extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dbpedia:update-books';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get books details from DBpedia.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->getBooksInfo();
	}

	protected function getBooksInfo()
	{
		$start_total = microtime(true);

		set_time_limit(0);

		include(app_path().'/includes/sparqllib.php');

		$db = sparql_connect( "http://dbpedia.org/sparql" );
		if( !$db ) {
			$this->error(sparql_errno() . ": " . sparql_error());
			exit;
		}

		sparql_ns( "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
		sparql_ns( "rdfs","http://www.w3.org/2000/01/rdf-schema#" );
		sparql_ns( "dbpedia","http://dbpedia.org/resource/" );
		sparql_ns( "dbpedia-owl","http://dbpedia.org/ontology/" );
		sparql_ns( "dbpprop","http://dbpedia.org/property/" );

		// pega todos os generos registrados
		$allGenres = Genre::lists('name', 'id');

		//$book_list = '';
		$books = Book::where('info_status', '=', 'outdated')->orderBy('id')->get();
		$count = 1;
		$total_books = $books->count();
		foreach($books as $book) {

			$patterns = array(
				'/ \(short story collection\)/',
				'/\(short story collection\)/',
				'/ \(series\)/',
				'/\(series\)/',
				'/ \(book\)/',
				'/\(book\)/',
				'/ \([A-Za-z]* novel\)/',
				'/\(novel\)/',
				'/ \(\d+ book\)/',
				'/\(\d+ book\)/',
				'/ \(\d+\)/'
			);
			$title = preg_replace($patterns, '', $book->title);
			$title_search = $output = rtrim($title, '.');

			$this->info($count++ . '/' .$total_books . ' ## ' . $title);
			$this->info('Getting details from DBpedia.');

			$sparql = '
                SELECT DISTINCT
					str(?name) as ?book_title
					str(?book_author) as ?book_author
					str(?book_country) as ?book_country
					str(?book_released) as ?book_released
					str(?book_abstract) as ?book_abstract
					?book_numberOfPages
					(group_concat(distinct ?book_genre;separator=", ") as ?book_genre)

					WHERE {
						{ ?book_title rdf:type <http://dbpedia.org/ontology/Book> }
						UNION
						{ ?book_title rdf:type <http://dbpedia.org/class/Book> }
						?book_title rdfs:comment ?book_abstract .
						?book_title dct:subject/rdfs:label ?book_genre .
						?book_title rdfs:label ?name
						OPTIONAL {
							{ ?book_title dbpedia-owl:author/rdfs:label ?book_author }
							UNION
							{ ?book_title dbp:author/rdfs:label ?book_author }
						}
						OPTIONAL {
							?book_title dbpprop:country ?book_country .
						}
						OPTIONAL {
							?book_title dbpprop:releaseDate ?book_released .
						}
						OPTIONAL {
							{ ?book_title dbpedia-owl:numberOfPages ?book_numberOfPages }
							UNION
							{ ?book_title dbpprop:pages ?book_numberOfPages }
						}
						FILTER(LANGMATCHES(LANG(?name), "en")) .
						FILTER(LANGMATCHES(LANG(?book_author), "en")) .
						FILTER(LANGMATCHES(LANG(?book_abstract), "en")) .
						FILTER regex(str(?name), "\\\b'.$title_search.'\\\b", "i")
					} LIMIT 10';
			//return var_dump($sparql);

			$result = sparql_query( $sparql );
			if( !$result ) {
				$this->error(sparql_errno() . ": " . sparql_error());
				exit;
			}

			// não achou nenhum livro
			if(count($result->rows) <= 0) {
				$this->info('Book NOT FOUND.' . PHP_EOL);
				continue;
			}

			// verifica cada resultado retornado pelo sparql
			while( $row = sparql_fetch_array( $result ) )
			{
				$db_title = preg_replace($patterns, '', $row['book_title']);
				if(mb_strtolower($db_title) == mb_strtolower($title)) {
					break;
				}
			}

			$genres = explode(',', $row['book_genre']);

			$ids = array();
			foreach($allGenres as $id => $genre){
				$matches = array_filter($genres, function($var) use ($genre) { return preg_match("/\b$genre\b/i", $var); });
				if(!empty($matches)){
					$ids[] = $id;
				}
			}

			// ano de quatro digitos
			if( isset($row['book_released']) && preg_match('/^[1-9]\d{3}$/', (int)$row['book_released']) ){
				$row['book_released'] = $row['book_released'] . '-01-01';
			}

			if(isset($row['book_country'])){
				$patterns = array(
					'/.*\//',
				);
				$country_name = preg_replace($patterns, '', $row['book_country']);
				$country_name = str_replace('_', ' ', $country_name);
				try{
					$country = Country::where('name', $country_name)->firstOrFail();
				}catch (ModelNotFoundException $e){
					$this->info('aahhh');
					$country = new Country();
					$country->name = $country_name;
					$country->save();
				}
				$book->countries_id = $country->id;
			}else{
				$book->countries_id = null;
			}

			// ano de quatro digitos
			if( isset($row['book_released']) && preg_match('/^[1-9]\d{3}$/', (int)$row['book_released']) ){
				$row['book_released'] = $row['book_released'] . '-01-01';
			}

			$book->author = isset($row['book_author']) ? $row['book_author'] : null;
			$book->abstract = $row['book_abstract'];
			$book->release_date = isset($row['book_released']) ? $row['book_released'] : null;
			$book->number_pages = isset($row['book_numberOfPages']) ? $row['book_numberOfPages'] : null;
			$book->info_status = 'updated';
			$book->save();
			$book->genres()->sync($ids);

			$this->info('Book updated.' . PHP_EOL);
			//$book_list .= $title . '<br>';
		}

		$time_elapsed_us = number_format((microtime(true) - $start_total),2,',','.');
		$this->info('Total Elapsed Time: ' . $time_elapsed_us . "s" . PHP_EOL);
		//return $book_list;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
