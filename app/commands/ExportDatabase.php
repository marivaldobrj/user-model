<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExportDatabase extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'export:database';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$db_name = $value = $this->argument('db_name');

		switch($db_name){
			case 'users_books':
				$this->usersBooks();
				break;
			case 'users_movies':
				$this->usersMovies();
				break;
			case 'users_genders':
				$this->usersGenders();
				break;
			case 'users_cities':
				$this->usersCities();
				break;
			case 'users_birthyear':
				$this->usersBirthyear();
				break;
			case 'books_genres':
				$this->booksGenres();
				break;
			case 'books_countries':
				$this->booksCountries();
				break;
			case 'movies_genres':
				$this->moviesGenres();
				break;
			case 'movies_countries':
				$this->moviesCountries();
				break;
			default:
				$this->info('This table could not be exported or was not found.');
		}
	}

	public function usersBooks()
	{
		$lines = DB::table('books_users')->orderBy('users_id')->get();
		$myfile = fopen(storage_path('app/users_books.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->users_id . ' ' . $line->books_id;
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function usersMovies()
	{
		$lines = DB::table('movies_users')->orderBy('users_id')->get();
		$myfile = fopen(storage_path('app/users_movies.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->users_id . ' ' . $line->movies_id;
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function booksGenres()
	{
		$lines = DB::table('books_genres')->orderBy('books_id')->get();
		$myfile = fopen(storage_path('app/books_genres.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->books_id . ' ' . $line->genres_id;
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function booksCountries()
	{
		$lines = DB::table('books')->orderBy('id')->get();
		$myfile = fopen(storage_path('app/books_countries.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			if($line->countries_id != null) {
				$txt = ($k > 0 ? PHP_EOL : '') . $line->id . ' ' . $line->countries_id;
				fwrite($myfile, $txt);
			}
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function moviesGenres()
	{
		$lines = DB::table('genres_movies')->orderBy('movies_id')->get();
		$myfile = fopen(storage_path('app/movies_genres.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->movies_id . ' ' . $line->genres_id;
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function moviesCountries()
	{
		$lines = DB::table('movies')->orderBy('id')->get();
		$myfile = fopen(storage_path('app/movies_countries.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			if($line->countries_id != null) {
				$txt = ($k > 0 ? PHP_EOL : '') . $line->id . ' ' . $line->countries_id;
				fwrite($myfile, $txt);
			}
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function usersGenders()
	{
		$lines = DB::table('users')->orderBy('id')->get();
		$myfile = fopen(storage_path('app/users_genders.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->id . ' ' . $line->genders_id;
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function usersCities()
	{
		$lines = DB::table('users')->orderBy('id')->get();
		$myfile = fopen(storage_path('app/users_cities.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->id . ' ' . $line->cities_id;
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	public function usersBirthyear()
	{
		$lines = DB::table('users')->orderBy('id')->get();
		$myfile = fopen(storage_path('app/users_birthyear.txt'), "w") or die("Unable to open file!");

		foreach ($lines as $k => $line)
		{
			$txt = ($k > 0 ? PHP_EOL : '') . $line->id . ' ' . date('Y', strtotime($line->birth_date));
			fwrite($myfile, $txt);
		}
		fclose($myfile);
		$this->info('Database exported.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('db_name', InputArgument::REQUIRED, 'The database to export.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
