<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

$logFile = 'log-'.php_sapi_name().'.txt';

Log::useDailyFiles(storage_path().'/logs/'.$logFile);

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);

	//Se tiver em producao e tiver erro, vamos mandar um email !
	if (App::environment('production') && !App::runningInConsole()) {
		$send_email = true;

		//extrair informações da exceção
		preg_match_all('/exception \'(.*?)\' in/s', $exception, $matches);

		$exception_data = explode("'",$matches[0][0]);
		$exception_name = $exception_data[1];
		$error_message = isset($exception_data[3]) ? $exception_data[3] : "";

		$ip = Request::server('REMOTE_ADDR');
		$host = getHostByAddr(Request::server('REMOTE_ADDR'));
		$url = Request::url();

		//Se a exceção for ModelNotFound, gera o erro 404 e não envia email
		if($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException){
			$code = 404;
		}
		
		if($code == 404 || ($exception instanceof ErrorException && $error_message == 'filemtime(): No such file or directory')){
			$send_email = false;
		}

		if ($send_email) {

			$data = array(
				'exception'         => $exception,
				'exception_name'    => $exception_name,
				'error_message'     => $error_message,
				'code'              => $code,
				'ip'                => $ip,
				'host'              => $host,
				'url'               => $url,
				'headers'           => getallheaders(),
				'request'           => Request::instance()
			);

			$subj = '[UserModel] Error - ' . $data['code'];
			Mail::send('emails.error', $data, function ($message) use ($subj) {
					$message->from('erros@saindo.com.br', 'UserModel')
						->to('marivaldorjunior@gmail.com', 'Marivaldo Junior')
						->subject($subj);
				}
			);
		}
	}
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';
