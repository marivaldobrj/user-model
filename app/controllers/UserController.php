<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends BaseController {

    public function showUserHome($user_id)
    {
        $user = User::findOrFail($user_id);

        return View::make('users.index')
            ->with('user', $user);
    }

    public function showUserMovies($user_id)
    {
        $user = User::findOrFail($user_id);

        $movies = $user->movies()->paginate(12);

        return View::make('users.movies')
            ->with('user', $user)
            ->with('movies', $movies);
    }

    public function showUserBooks($user_id)
    {
        $user = User::findOrFail($user_id);

        $books = $user->books()->paginate(12);

        return View::make('users.books')
            ->with('user', $user)
            ->with('books', $books);
    }

    public function showUserProfile($user_id){
        $user = User::findOrFail($user_id);

        $movies_genres = $user->genresMoviesPercents();

        $sum = 0;
        foreach ($movies_genres as $genre) {
            $sum += intval($genre->qtd);
        }

        $moviesGenresWithPercent = [];
        foreach($movies_genres as $genre) {
            $obj = new stdClass();
            $obj->name = $genre->name;
            $obj->percent =  round($genre->qtd / $sum * 100, 1) . '%';
            $moviesGenresWithPercent[] = $obj;
        }

        $books_genres = $user->genresBooksPercents();

        $sum = 0;
        foreach ($books_genres as $genre) {
            $sum += intval($genre->qtd);
        }

        $booksGenresWithPercent = [];
        foreach($books_genres as $genre) {
            $obj = new stdClass();
            $obj->name = $genre->name;
            $obj->percent =  round($genre->qtd / $sum * 100, 1) . '%';
            $booksGenresWithPercent[] = $obj;
        }

        return View::make('users.profile')
            ->with('movies_genres', $moviesGenresWithPercent)
            ->with('books_genres', $booksGenresWithPercent)
            ->with('user', $user);
    }

    /**
     * Do login with facebook
     *
     * @return mixed
     */
    public function loginFacebook()
    {
        set_time_limit(0);

        // get data from input
        $code = Input::get( 'code' );

        // get fb service
        $fb = OAuth::consumer( 'Facebook' );

        // check if code is valid

        // if code is provided get user data and sign in
        if ( !empty( $code ) ) {

            // This was a callback request from facebook, get the token
            $token = $fb->requestAccessToken( $code );

            // Send a request with it
            $result = json_decode( $fb->request( '/me?fields=id,name,first_name,email,gender,birthday,picture.type(large),video.watches,video.rates,books.reads' ), true );
            //return $result;
            $user = User::where('facebook_id','=',$result['id'])->first();
            if($user){
                // user already exists, login user
                Auth::login($user);
                // if response has picture, always update picture on logon
                if($result["picture"]["data"]["url"])
                    $user->facebook_image = $result["picture"]["data"]["url"];
            }else{
                // user not found on application, new user

                // create user
                $user = new User();
                $user->name = $result['name'];
                $user->first_name = $result['first_name'];
                $user->email = $result['email'];
                if (array_key_exists("gender", $result))
                    $gender = Gender::where('gender', $result['gender'])->first();
                    if($gender) {
                        $user->genders_id = $gender->id;
                    }
                if (array_key_exists("birthday", $result))
                    $user->birth_date = date( "Y-m-d", strtotime( $result['birthday']) );
                $user->facebook_id = $result["id"];
                $user->facebook_image = $result["picture"]["data"]["url"];

                $user->save();

                // login the user with remember option
                Auth::login($user, true);
            }

            return Redirect::to('users/'.$user->id.'/profile');

            //Var_dump
            //display whole array().
            //dd($result);
        }
        // if not ask for permission first
        else {

            // get fb authorization
            $url = $fb->getAuthorizationUri();

            // return to facebook login url
            return Redirect::to( (string)$url );
        }
    }

    /**
     * Logout
     *
     * @access   public
     * @return   Redirect
     */
    public function logout()
    {
        // Sai
        //
        Auth::logout();
        Session::flush();

        return Redirect::to('login');
    }

    public function getFacebookData()
    {
        $user = Auth::user();

        if( $user->facebook_data_status == 'not_synced'){
            set_time_limit(0);

            // get fb service
            $fb = OAuth::consumer( 'Facebook' );
            $result = json_decode( $fb->request( '/me?fields=id,name,first_name,email,gender,birthday,picture.type(large),video.watches,video.rates,books.reads' ), true );

            $movies_response = $result["video.watches"];
            while( count($movies_response['data']) )
            {
                $movies = array();
                foreach($movies_response['data'] as $movie){
                    try {
                        // verifica se ja existe o filme no banco
                        $the_movie = Movie::where('title', $movie['data']['movie']['title'])->firstOrFail();
                    }catch (ModelNotFoundException $e){
                        // se nao existe o filme no banco, cria
                        $the_movie = new Movie();
                        $the_movie->title = $movie['data']['movie']['title'];
                        $the_movie->facebook_id = $movie['data']['movie']['id'];
                        $the_movie->facebook_url = $movie['data']['movie']['url'];

                        // pega o nome da pagina do filme no facebook
                        $movie_fb_name = explode('/', $the_movie->facebook_url);
                        $movie_fb_name = end($movie_fb_name);

                        // pega os dados do filme no facebook
                        $movie_info = json_decode( $fb->request( '/'.$movie_fb_name ), true );

                        if(isset($movie_info['release_date'])) {
                            $time = strtotime($movie_info['release_date']);
                            $release_date = ($time === false) ? null : date('Y-m-d', $time);
                            //preg_match_all('/(\d{4})/', $movie_info, $matches);
                            if($release_date){
                                $the_movie->release_date = $release_date;
                            }
                        }
                    }

                    $movies[] = $the_movie;
                }
                $user->movies()->saveMany($movies);

                // se nao tem outra pagina de filmes para requisitar termina o loop
                if( !isset($movies_response['paging']['next']) ){
                    break;
                }

                $url_pieces = explode('/', $movies_response['paging']['next']);
                $url = $url_pieces[count($url_pieces)-1];
                $movies_response = json_decode( $fb->request( '/' . $user->facebook_id . '/' . $url ), true );
            }


            $books_response = $result["books.reads"];
            while( count($movies_response['data']) )
            {
                $books = array();
                foreach($books_response['data'] as $book){
                    try {
                        // verifica se ja existe o livro no banco
                        $the_book = Book::where('title', $book['data']['book']['title'])->firstOrFail();
                    }catch (ModelNotFoundException $e){
                        // se nao existe o livro no banco, cria
                        $the_book = new Book();
                        $the_book->title = $book['data']['book']['title'];
                        $the_book->facebook_id = $book['data']['book']['id'];
                        $the_book->facebook_url = $book['data']['book']['url'];
                    }

                    $books[] = $the_book;
                }
                $user->books()->saveMany($books);

                // se nao tem outra pagina de livros para requisitar termina o loop
                if(!isset($books_response['paging']['next'])){
                    break;
                }

                $url_pieces = explode('/', $books_response['paging']['next']);
                $url = $url_pieces[count($url_pieces)-1];
                $books_response = json_decode( $fb->request( '/' . $user->facebook_id . '/' . $url ), true );
            }

            $user->facebook_data_status = 'synced';
            $user->save();
        }

        return Redirect::to('users/'.$user->id.'/profile');
    }

    public function proc()
    {
        $cmd = "C:\\Users\\Junior\\xampp\\php\\php.exe artisan migrate";

        $cwd = "C:\\Users\\Junior\\xampp\\htdocs\\usermodel";
        chdir($cwd);
        if (substr(php_uname(), 0, 7) == "Windows"){
            pclose(popen("start /B ". $cmd, "r"));
        }
        else {
            exec($cmd . " > /dev/null &");
        }

        //$process = new \Symfony\Component\Process\Process($cmd, $cwd);
        //$process->start();
    }
}
