<?php

class MovieController extends BaseController
{

    public function showMovie($movie_id)
    {
        $movie = Movie::findOrFail($movie_id);

        return View::make('movie')
            ->with('movie', $movie);
    }

}