<?php
/**
 * Created by PhpStorm.
 * User: Junior
 * Date: 31/03/2015
 * Time: 07:15
 */

class ProfileController extends BaseController
{
    public function showUserProfile($user_id)
    {
        $user = User::findOrFail($user_id);

        return View::make('users.profile')
            ->with('user', $user);
    }

    public function getMoviesInfo()
    {
        set_time_limit(0);

        include(app_path().'\includes\sparqllib.php');

        $db = sparql_connect( "http://dbpedia.org/sparql" );
        if( !$db ) {
            return sparql_errno() . ": " . sparql_error(). "\n"; exit;
        }

        sparql_ns( "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
        sparql_ns( "rdfs","http://www.w3.org/2000/01/rdf-schema#" );
        sparql_ns( "dbpedia","http://dbpedia.org/resource/" );
        sparql_ns( "dbpedia-owl","http://dbpedia.org/ontology/" );
        sparql_ns( "dbpprop","http://dbpedia.org/property/" );

        // pega todos os generos registrados
        $allGenres = Genre::lists('name', 'id');

        $movie_list = '';
        $movies = Movie::where('info_status', '=', 'outdated')->get();
        foreach($movies as $movie) {

            $patterns = array(
                '/ \(film series\)/',
                '/\(film series\)/',
                '/ \(film\)/',
                '/\(film\)/',
                '/ \(\d+ film\)/',
                '/\(\d+ film\)/',
                '/ \(\d+\)/'
            );
            $title = preg_replace($patterns, '', $movie->title);

            $sparql = '
                SELECT DISTINCT
                    str(?name) as ?film_title
                    str(?film_director) as ?film_director
                    str(?film_country) as ?film_country
                    str(?film_released) as ?film_released
                    str(?film_abstract) as ?film_abstract
                    ?film_runtime
                    (group_concat(distinct ?film_genre;separator=",") as ?film_genre)

                    WHERE {
                        ?film_title rdf:type <http://dbpedia.org/ontology/Film> .
                        ?film_title rdfs:comment ?film_abstract .
                        ?film_title dcterms:subject/rdfs:label ?film_genre .
                        ?film_title rdfs:label ?name
                        OPTIONAL {
                            ?film_title dbpedia-owl:director/rdfs:label ?film_director .
                            ?film_title dbpprop:country ?film_country .
                            ?film_title dbpprop:released ?film_released .
                            ?film_title dbpedia-owl:runtime ?film_runtime .
                        }
                        FILTER(LANGMATCHES(LANG(?name), "en")) .
                        FILTER(LANGMATCHES(LANG(?film_director), "en")) .
                        FILTER(LANGMATCHES(LANG(?film_abstract), "en")) .
                        FILTER contains(str(?name), "'.$title.'")
                    } LIMIT 1';
            //return var_dump($sparql);

            $result = sparql_query( $sparql );
            if( !$result ) {
                return sparql_errno() . ": " . sparql_error(). "\n"; exit;
            }

            $row = sparql_fetch_array( $result );

            $genres = explode(',', $row['film_genre']);

            $ids = array();
            foreach($allGenres as $id => $genre){
                $matches = array_filter($genres, function($var) use ($genre) { return preg_match("/\b$genre\b/i", $var); });
                if(!empty($matches)){
                    $ids[] = $id;
                }
            }

            $movie->director = $row['film_director'];
            $movie->country = $row['film_country'];
            $movie->release_date = $row['film_released'];
            $movie->abstract = $row['film_abstract'];
            $movie->info_status = 'updated';
            $movie->save();
            $movie->genres()->sync($ids);

            $movie_list .= $title . '<br>';
        }

        //$this->comunidades()->attach($comunidade->id, array('tipo_usuario_id' => $tipo));

        //return 'movies info updated.';
        return $movie_list;
    }

    public function getBooksInfo()
    {
        set_time_limit(0);

        include(app_path().'\includes\sparqllib.php');

        $db = sparql_connect( "http://dbpedia.org/sparql" );
        if( !$db ) {
            return sparql_errno() . ": " . sparql_error(). "\n"; exit;
        }

        sparql_ns( "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
        sparql_ns( "rdfs","http://www.w3.org/2000/01/rdf-schema#" );
        sparql_ns( "dbpedia","http://dbpedia.org/resource/" );
        sparql_ns( "dbpedia-owl","http://dbpedia.org/ontology/" );
        sparql_ns( "dbpprop","http://dbpedia.org/property/" );

        // pega todos os generos registrados
        $allGenres = Genre::lists('name', 'id');

        $book_list = '';
        $books = Book::where('info_status', '=', 'outdated')->get();
        foreach($books as $book) {
            $patterns = array(
                '/ \(short story collection\)/',
                '/\(short story collection\)/',
                '/ \(series\)/',
                '/\(series\)/',
                '/ \(book\)/',
                '/\(book\)/',
                '/ \(novel\)/',
                '/\(novel\)/',
                '/ \(\d+ film\)/',
                '/\(\d+ film\)/',
                '/ \(\d+\)/'
            );
            $title = preg_replace($patterns, '', $book->title);

            $sparql = '
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX dbpedia: <http://dbpedia.org/resource/>
                PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
                PREFIX dbpprop: <http://dbpedia.org/property/>

                SELECT DISTINCT
                str(?name) as ?book_title
                str(?book_author) as ?book_author
                str(?book_country) as ?book_country
                str(?book_released) as ?book_released
                str(?book_abstract) as ?book_abstract
                ?book_numberOfPages
                (group_concat(distinct ?book_genre;separator=", ") as ?book_genre)
                WHERE {
                    ?book_title rdf:type <http://dbpedia.org/ontology/Book> .
                    ?book_title rdfs:comment ?book_abstract .
                    ?book_title dcterms:subject/rdfs:label ?book_genre .
                    ?book_title rdfs:label ?name
                    OPTIONAL {
                        ?book_title dbpedia-owl:author/rdfs:label ?book_author .
                        ?book_title dbpprop:country ?book_country .
                        ?book_title dbpprop:releaseDate ?book_released .
                        ?book_title dbpedia-owl:numberOfPages ?book_numberOfPages .
                    }
                    FILTER(LANGMATCHES(LANG(?name), "en")) .
                    FILTER(LANGMATCHES(LANG(?book_author), "en")) .
                    FILTER(LANGMATCHES(LANG(?book_abstract), "en")) .
                    FILTER contains(str(?name), "'.$title.'")
                } LIMIT 1';
            //return var_dump($sparql);

            $result = sparql_query( $sparql );
            if( !$result ) {
                return sparql_errno() . ": " . sparql_error(). "\n"; exit;
            }

            $row = sparql_fetch_array( $result );

            $genres = explode(',', $row['book_genre']);

            $ids = array();

            foreach($allGenres as $id => $genre){
                $matches = array_filter($genres, function($var) use ($genre) { return preg_match("/\b$genre\b/i", $var); });
                if(!empty($matches)){
                    $ids[] = $id;
                }
            }

            $book->author = $row['book_author'];
            $book->abstract = $row['book_abstract'];
            $book->country = $row['book_country'];
            $book->release_date = $row['book_released'];
            $book->number_pages = $row['book_numberOfPages'];
            $book->info_status = 'updated';
            $book->save();
            $book->genres()->sync($ids);

            $book_list .= $title . '<br>';
        }

        return $book_list;
    }
}