<?php

class BookController extends BaseController
{

    public function showBook($book_id)
    {
        $book = Book::findOrFail($book_id);

        return View::make('book')
            ->with('book', $book);
    }

}