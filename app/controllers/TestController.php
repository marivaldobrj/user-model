<?php

class TestController extends BaseController {

    public function getMoviesInfoFromDBPedia()
    {

        $start_total = microtime(true);

        set_time_limit(0);

        include(app_path().'/includes/sparqllib.php');

        $db = sparql_connect( "http://dbpedia.org/sparql" );
        if( !$db ) {
            $this->error(sparql_errno() . ": " . sparql_error());
            exit;
        }

        sparql_ns( "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
        sparql_ns( "rdfs","http://www.w3.org/2000/01/rdf-schema#" );
        sparql_ns( "dbpedia","http://dbpedia.org/resource/" );
        sparql_ns( "dbpedia-owl","http://dbpedia.org/ontology/" );
        sparql_ns( "dbpprop","http://dbpedia.org/property/" );

        // pega todos os generos registrados
        $allGenres = Genre::lists('name', 'id');

        //$movie_list = '';
        $movies = Movie::where('info_status', '=', 'outdated')->orderBy('id')->get();
        $count = 1;
        $total_movies = $movies->count();
        foreach($movies as $movie) {

            $patterns = array(
                '/ \(film series\)/',
                '/\(film series\)/',
                '/ \(film\)/',
                '/\(film\)/',
                '/ \(\d+ film\)/',
                '/\(\d+ film\)/',
                '/ \(\d+\)/'
            );
            $title = preg_replace($patterns, '', $movie->title);
            $title_search = $output = rtrim($title, '.');

            //$this->info($count++ . '/' .$total_movies . ' ## ' . $title);
            //$this->info('Getting details from DBpedia.');

            $sparql = '
                SELECT DISTINCT
                    str(?name) as ?film_title
                    str(?film_director) as ?film_director
                    str(?film_country) as ?film_country
                    str(?film_released) as ?film_released
                    str(?film_abstract) as ?film_abstract
                    ?film_runtime
                    (group_concat(distinct ?film_genre;separator=",") as ?film_genre)

                    WHERE {
                        { ?film_title rdf:type <http://dbpedia.org/ontology/Film> }
                        UNION
                        { ?film_title rdf:type <http://dbpedia.org/class/yago/AmericanFilms> }
                        ?film_title rdfs:comment ?film_abstract .
                        ?film_title dct:subject/rdfs:label ?film_genre .
                        ?film_title rdfs:label ?name
                        OPTIONAL {
                             { ?film_title dbpedia-owl:director/rdfs:label ?film_director }
                             UNION
                             { ?film_title dbp:director/rdfs:label ?film_director }
                        }
                        OPTIONAL {
                        	?film_title dbpprop:country ?film_country .
                        }
                        OPTIONAL {
                        	?film_title dbpprop:released ?film_released .
                        }
                        OPTIONAL {
                        	?film_title dbpedia-owl:runtime ?film_runtime .
                        }
                        FILTER(LANGMATCHES(LANG(?name), "en")) .
                        FILTER(LANGMATCHES(LANG(?film_director), "en")) .
                        FILTER(LANGMATCHES(LANG(?film_abstract), "en")) .
                        FILTER regex(str(?name), "\\\bSuperbad\\\b", "i")
                    } LIMIT 5';
            //return var_dump($sparql);
            //FILTER contains(str(?name), "'.$title.'")
            //FILTER regex(str(?name), "\\\b'.$title.'\\\b", "i")


            $result = sparql_query( $sparql );
            if( !$result ) {
                $this->error(sparql_errno() . ": " . sparql_error());
                exit;
            }

            return View::make('test')->with('data',$result);

            while( $row = sparql_fetch_array( $result ) )
            {
                $db_title = preg_replace($patterns, '', $row['film_title']);

                if(mb_strtolower($db_title) == mb_strtolower($title)) {
                    return View::make('test')->with('data',$row);
                }
            }

            $genres = explode(',', $row['film_genre']);

            $ids = array();
            foreach($allGenres as $id => $genre){
                $matches = array_filter($genres, function($var) use ($genre) { return preg_match("/\b$genre\b/i", $var); });
                if(!empty($matches)){
                    $ids[] = $id;
                }
            }

            $movie->director = isset($row['film_director']) ? $row['film_director'] : null;
            $movie->country = isset($row['film_country']) ? $row['film_country'] : null;
            $movie->release_date = isset($row['film_released']) ? $row['film_released'] : null;
            $movie->abstract = $row['film_abstract'];
            //$movie->info_status = 'updated';
            $movie->save();
            $movie->genres()->sync($ids);

            //$this->info('Movie updated.' . PHP_EOL);

            return View::make('test')->with('data',$result);
            //$movie_list .= $title . '<br>';
        }

        $time_elapsed_us = number_format((microtime(true) - $start_total),2,',','.');
        $this->info('Total Elapsed Time: ' . $time_elapsed_us . "s" . PHP_EOL);
    }

    public function getBooksInfoFromDBPedia()
    {
        $start_total = microtime(true);

        set_time_limit(0);

        include(app_path().'/includes/sparqllib.php');

        $db = sparql_connect( "http://dbpedia.org/sparql" );
        if( !$db ) {
            $this->error(sparql_errno() . ": " . sparql_error());
            exit;
        }

        sparql_ns( "rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#" );
        sparql_ns( "rdfs","http://www.w3.org/2000/01/rdf-schema#" );
        sparql_ns( "dbpedia","http://dbpedia.org/resource/" );
        sparql_ns( "dbpedia-owl","http://dbpedia.org/ontology/" );
        sparql_ns( "dbpprop","http://dbpedia.org/property/" );

        // pega todos os generos registrados
        $allGenres = Genre::lists('name', 'id');

        //$book_list = '';
        $books = Book::where('info_status', '=', 'outdated')->orderBy('id')->get();
        $count = 1;
        $total_books = $books->count();
        foreach($books as $book) {

            $patterns = array(
                '/ \(short story collection\)/',
                '/\(short story collection\)/',
                '/ \(series\)/',
                '/\(series\)/',
                '/ \(book\)/',
                '/\(book\)/',
                '/ \(novel\)/',
                '/\(novel\)/',
                '/ \(\d+ film\)/',
                '/\(\d+ film\)/',
                '/ \(\d+\)/'
            );
            $title = preg_replace($patterns, '', $book->title);
            $title_search = $output = rtrim($title, '.');

            $this->info($count++ . '/' .$total_books . ' ## ' . $title);
            $this->info('Getting details from DBpedia.');

            $sparql = '
                SELECT DISTINCT
					str(?name) as ?book_title
					str(?book_author) as ?book_author
					str(?book_country) as ?book_country
					str(?book_released) as ?book_released
					str(?book_abstract) as ?book_abstract
					?book_numberOfPages
					(group_concat(distinct ?book_genre;separator=", ") as ?book_genre)

					WHERE {
						{ ?book_title rdf:type <http://dbpedia.org/ontology/Book> }
						UNION
						{ ?book_title rdf:type <http://dbpedia.org/class/Book> }
						?book_title rdfs:comment ?book_abstract .
						?book_title dct:subject/rdfs:label ?book_genre .
						?book_title rdfs:label ?name
						OPTIONAL {
							{ ?book_title dbpedia-owl:author/rdfs:label ?book_author }
							UNION
							{ ?book_title dbp:author/rdfs:label ?book_author }
						}
						OPTIONAL {
							?book_title dbpprop:country ?book_country .
						}
						OPTIONAL {
							?book_title dbpprop:releaseDate ?book_released .
						}
						OPTIONAL {
							{ ?book_title dbpedia-owl:numberOfPages ?book_numberOfPages }
							UNION
							{ ?book_title dbpprop:pages ?book_numberOfPages }
						}
						FILTER(LANGMATCHES(LANG(?name), "en")) .
						FILTER(LANGMATCHES(LANG(?book_author), "en")) .
						FILTER(LANGMATCHES(LANG(?book_abstract), "en")) .
						FILTER regex(str(?name), "\\\b'.$title_search.'\\\b", "i")
					} LIMIT 5';
            //return var_dump($sparql);

            $result = sparql_query( $sparql );
            if( !$result ) {
                $this->error(sparql_errno() . ": " . sparql_error());
                exit;
            }

            // n�o achou nenhum livro
            if(count($result->rows) <= 0) {
                $this->info('Book NOT FOUND.' . PHP_EOL);
                continue;
            }

            while( $row = sparql_fetch_array( $result ) )
            {
                $db_title = preg_replace($patterns, '', $row['book_title']);
                if(mb_strtolower($db_title) == mb_strtolower($title)) {
                    break;
                }
            }

            $genres = explode(',', $row['book_genre']);

            $ids = array();
            foreach($allGenres as $id => $genre){
                $matches = array_filter($genres, function($var) use ($genre) { return preg_match("/\b$genre\b/i", $var); });
                if(!empty($matches)){
                    $ids[] = $id;
                }
            }

            // ano de quatro digitos
            if( isset($row['book_released']) && preg_match('/^[1-9]\d{3}$/', (int)$row['book_released']) ){
                $row['book_released'] = $row['book_released'] . '-01-01';
            }

            $book->author = isset($row['book_author']) ? $row['book_author'] : null;
            $book->abstract = $row['book_abstract'];
            $book->country = isset($row['book_country']) ? $row['book_country'] : null;
            $book->release_date = isset($row['book_released']) ? $row['book_released'] : null;
            $book->number_pages = isset($row['book_numberOfPages']) ? $row['book_numberOfPages'] : null;
            //$book->info_status = 'updated';
            $book->save();
            $book->genres()->sync($ids);

            $this->info('Book updated.' . PHP_EOL);
            //$book_list .= $title . '<br>';
        }

        $time_elapsed_us = number_format((microtime(true) - $start_total),2,',','.');
        $this->info('Total Elapsed Time: ' . $time_elapsed_us . "s" . PHP_EOL);
    }
}
