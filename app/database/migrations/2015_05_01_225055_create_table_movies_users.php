<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMoviesUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movies_users', function(Blueprint $table)
		{
			$table->integer('movies_id')->unsigned();
			$table->integer('users_id')->unsigned();

			$table->foreign('movies_id')
				->references('id')->on('movies')
				->onDelete('restrict');

			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movies_users');
	}

}
