<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMovies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title',255);
			$table->string('director',255)->nullable();
			$table->string('country',255)->nullable();
			$table->date('release_date')->nullable();
			$table->text('abstract')->nullable();
			$table->integer('rating')->nullable();
			$table->enum('status', array('watched', 'want_to_watch'));
			$table->string('facebook_id');
			$table->string('facebook_url');
			$table->string('dbpedia_url')->nullable();
            $table->enum('info_status', array('outdated', 'updated'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movies');
	}

}
