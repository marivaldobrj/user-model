<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGenresMovies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('genres_movies', function(Blueprint $table)
		{
			$table->integer('genres_id')->unsigned();
			$table->integer('movies_id')->unsigned();

			$table->foreign('genres_id')
				->references('id')->on('genres')
				->onDelete('cascade');

			$table->foreign('movies_id')
				->references('id')->on('movies')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('genres_movies');
	}

}
