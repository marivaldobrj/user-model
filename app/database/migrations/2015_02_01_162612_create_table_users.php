<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',255);
			$table->string('first_name',255);
			$table->string('email',255)->unique();
			$table->string('password')->nullable();
			$table->rememberToken();
			$table->string('gender',50)->nullable();
			$table->date('birth_date',255)->nullable();
			$table->string('facebook_id');
			$table->string('facebook_image')->nullable();
			$table->enum('dbpedia_data_status', array('none', 'movies', 'books', 'all'));
			$table->enum('facebook_data_status', array('not_synced', 'synced'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
