<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title',255);
			$table->string('author',255)->nullable();
			$table->string('country',255)->nullable();
			$table->date('release_date')->nullable();
			$table->text('abstract')->nullable();
			$table->integer('number_pages')->unsigned()->nullable();
			$table->integer('rating')->nullable();
			$table->enum('status', array('read', 'want_to_read'));
			$table->string('facebook_id');
			$table->string('facebook_url');
			$table->string('dbpedia_url')->nullable();
			$table->enum('info_status', array('outdated', 'updated'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}

}
