<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooksUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books_users', function(Blueprint $table)
		{
			$table->integer('books_id')->unsigned();
			$table->integer('users_id')->unsigned();

			$table->foreign('books_id')
				->references('id')->on('books')
				->onDelete('restrict');

			$table->foreign('users_id')
				->references('id')->on('users')
				->onDelete('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books_users');
	}

}
