<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooksGenres extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books_genres', function(Blueprint $table)
		{
			$table->integer('genres_id')->unsigned();
			$table->integer('books_id')->unsigned();

			$table->foreign('genres_id')
				->references('id')->on('genres')
				->onDelete('cascade');

			$table->foreign('books_id')
				->references('id')->on('books')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books_genres');
	}

}
