<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Eloquent::unguard();

		$start_total = microtime(true);

		$this->command->info('Seeding Genre...');
		$start = microtime(true);
		$this->call('GenreSeeder');
		$time_elapsed_us = number_format((microtime(true) - $start),2,',','.');
		$this->command->info('Seeding Time: ' . $time_elapsed_us . "s" . PHP_EOL);

		$time_elapsed_us = number_format((microtime(true) - $start_total),2,',','.');
		$this->command->info('Total Seeding Time: ' . $time_elapsed_us . "s" . PHP_EOL);
	}

}
