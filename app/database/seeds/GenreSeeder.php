<?php
/**
 * Created by PhpStorm.
 * User: Junior
 * Date: 31/03/2015
 * Time: 07:16
 */

class GenreSeeder  extends Seeder {

    public function run()
    {
        DB::table('genres')->truncate();

        Genre::create(
            array(
                'name' => 'Action'
            )
        );

        Genre::create(
            array(
                'name' => 'Adventure'
            )
        );

        Genre::create(
            array(
                'name' => 'Comedy'
            )
        );

        Genre::create(
            array(
                'name' => 'Crime'
            )
        );

        Genre::create(
            array(
                'name' => 'Fiction'
            )
        );

        Genre::create(
            array(
                'name' => 'Fantasy'
            )
        );

        Genre::create(
            array(
                'name' => 'Historical'
            )
        );

        Genre::create(
            array(
                'name' => 'Horror'
            )
        );

        Genre::create(
            array(
                'name' => 'Mystery'
            )
        );

        Genre::create(
            array(
                'name' => 'Paranoid'
            )
        );

        Genre::create(
            array(
                'name' => 'Musical'
            )
        );

        Genre::create(
            array(
                'name' => 'Political'
            )
        );

        Genre::create(
            array(
                'name' => 'Romance'
            )
        );

        Genre::create(
            array(
                'name' => 'Saga'
            )
        );

        Genre::create(
            array(
                'name' => 'Satire'
            )
        );

        Genre::create(
            array(
                'name' => 'Science fiction'
            )
        );

        Genre::create(
            array(
                'name' => 'Slice of Life'
            )
        );

        Genre::create(
            array(
                'name' => 'Speculative'
            )
        );

        Genre::create(
            array(
                'name' => 'Thriller'
            )
        );

        Genre::create(
            array(
                'name' => 'Urban'
            )
        );

        Genre::create(
            array(
                'name' => 'Animation'
            )
        );

        Genre::create(
            array(
                'name' => 'Epic'
            )
        );

        Genre::create(
            array(
                'name' => 'War'
            )
        );

        Genre::create(
            array(
                'name' => 'Western'
            )
        );

        Genre::create(
            array(
                'name' => 'Sport'
            )
        );

        Genre::create(
            array(
                'name' => 'Biography'
            )
        );

        Genre::create(
            array(
                'name' => 'Superhero'
            )
        );

        Genre::create(
            array(
                'name' => 'Live Action'
            )
        );

        Genre::create(
            array(
                'name' => 'Romantic'
            )
        );

        Genre::create(
            array(
                'name' => 'Monster'
            )
        );

        Genre::create(
            array(
                'name' => 'Psychological'
            )
        );

        Genre::create(
            array(
                'name' => 'Psychiatry'
            )
        );

        Genre::create(
            array(
                'name' => 'Documentary'
            )
        );

        Genre::create(
            array(
                'name' => 'Teen'
            )
        );

        Genre::create(
            array(
                'name' => 'Independent'
            )
        );

        Genre::create(
            array(
                'name' => 'Disaster'
            )
        );

        Genre::create(
            array(
                'name' => 'Supernatural'
            )
        );

        Genre::create(
            array(
                'name' => 'Golden Globe'
            )
        );

        Genre::create(
            array(
                'name' => 'Academy Award'
            )
        );
    }
}