<?php 

return array( 
	
	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session', 

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Facebook
		 */
        'Facebook' => array(
            'client_id'     => '1526521660963957',
            'client_secret' => '3a3efae639c4708cec19d0fc7fe3ad54',
            'scope'         => array('email', 'user_birthday', 'user_actions.video', 'user_actions.books'),
        ),		

	)

);