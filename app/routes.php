<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('getMoviesInfoFromDBPedia', 'TestController@getMoviesInfoFromDBPedia');
Route::get('getBooksInfoFromDBPedia', 'TestController@getBooksInfoFromDBPedia');

Route::get('login', 'HomeController@showLogin');
Route::get('user/logout', 'UserController@logout');

Route::get('user/facebook', 'UserController@loginFacebook');

Route::get('user/facebook-data', 'UserController@getFacebookData');
Route::get('proc', 'UserController@proc');

Route::get('privacy-policy', 'HomeController@showPrivacyPolcicy');

Route::get('movies/{movie_id}', 'MovieController@showMovie');
Route::get('books/{book_id}', 'BookController@showBook');

Route::group(array('before' => 'auth'), function()
{
    Route::get('/', 'HomeController@showIndex');

    Route::group(array('prefix' => 'users'), function(){

        Route::get('{user_id}', 'UserController@showUserHome');
        Route::get('{user_id}/movies', 'UserController@showUserMovies');
        Route::get('{user_id}/books', 'UserController@showUserBooks');
        Route::get('{user_id}/profile', 'UserController@showUserProfile');
    });
});

Route::get('get-movies-info','ProfileController@getMoviesInfo');
Route::get('get-books-info','ProfileController@getBooksInfo');

Route::get('template', function(){
    return View::make('template');
});