<?php


class Movie extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'movies';

    public function user()
    {
        return $this->belongsTo('User');
    }

    /***
     * Return all genres from a genre
     */
    public function genres()
    {
        return $this->belongsToMany('Genre', 'genres_movies', 'movies_id', 'genres_id');
    }

    public function getYearAttribute(){
        if(!$this->released or $this->released == '0000-00-00')
            return null;
        $date = DateTime::createFromFormat("Y-m-d", $this->released);

        return $date->format("Y");
    }

    public function genresToString(){
        return implode(', ', $this->genres->lists('name'));
    }

    public function country()
    {
        return $this->belongsTo('Country', 'countries_id');
    }
}
