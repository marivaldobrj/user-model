<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Movies from the user
	 *
	 * @return mixed
	 */
	public function movies()
	{
		return $this->belongsToMany('Movie', 'movies_users', 'users_id', 'movies_id');
	}

	/**
	 * Books from the user
	 *
	 * @return mixed
	 */
	public function books()
	{
		return $this->belongsToMany('Book', 'books_users', 'users_id', 'books_id');
	}

    public function genresMoviesPercents()
	{
        return DB::select(DB::raw("
            select g.name as name, count(g.id) as qtd from users as u
            inner join movies_users as m_u
            on u.id = m_u.users_id
            inner join genres_movies as gm
            on gm.movies_id = m_u.movies_id
            inner join genres as g
            on g.id = gm.genres_id
            where u.id = ?
            group by g.id
            order by qtd desc"), array($this->id));
    }

    public function genresBooksPercents()
	{
        return DB::select(DB::raw("
            select g.name as name, count(g.id) as qtd
            from users as u
            inner join books_users as b_u
            on u.id = b_u.users_id
            inner join books_genres as bg
            on bg.books_id = b_u.books_id
            inner join genres as g
            on g.id = bg.genres_id
            where u.id = ?
            group by g.id
            order by qtd desc"), array($this->id));
    }
}
