<?php


class Genre extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'genres';

    /***
     * Return all movies from a genre
     */
    public function movies()
    {
        return $this->belongsToMany('Movie');
    }

    /***
     * Return all movies from a genre
     */
    public function books()
    {
        return $this->belongsToMany('Book');
    }

}
